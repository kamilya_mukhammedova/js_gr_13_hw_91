const express = require('express');
const cors = require('cors');
const {nanoid} = require('nanoid');
const app = express();
require('express-ws')(app);
const port = 8053;

app.use(cors());

const activeConnections = {};
const savedCoordinates = [];

app.ws('/draw', (ws, req) => {
  const id = nanoid();
  console.log('Client connected, id = ', id);
  activeConnections[id] = ws;

  ws.send(JSON.stringify({
    type: 'PREVIOUS_PIXELS',
    coordinates: savedCoordinates
  }));

  ws.on('message', msg => {
    const decodedMessage = JSON.parse(msg);
    switch (decodedMessage.type) {
      case 'SEND_PIXEL':
        Object.keys(activeConnections).forEach(id => {
          const connection = activeConnections[id];
          savedCoordinates.push(decodedMessage.coordinates);
          connection.send(JSON.stringify({
            type: 'NEW_PIXEL',
            circleCoordinates: decodedMessage.coordinates
          }));
        });
        break;
      default:
        console.log('Unknown type', decodedMessage.type);
    }
  });

  ws.on('close', () => {
    console.log('Client disconnected, id = ', id);
    delete activeConnections[id];
  });
});

app.listen(port, () => {
  console.log(`Server started on port ${port}`);
});