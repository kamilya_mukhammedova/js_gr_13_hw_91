import { AfterViewInit, Component, ElementRef, OnDestroy, ViewChild } from '@angular/core';

interface Circle {
  x: number,
  y: number,
  color: string
}

interface ServerMessage {
  type: string,
  coordinates: Circle[],
  circleCoordinates: Circle
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy, AfterViewInit {
  @ViewChild('canvas') canvas!: ElementRef;
  ws!: WebSocket;
  color = 'green';

  ngAfterViewInit(): void {
    const canvas: HTMLCanvasElement = this.canvas.nativeElement;
    this.ws = new WebSocket('ws://localhost:8053/draw');
    this.ws.onclose = () => console.log('WS closed !');

    this.ws.onmessage = event => {
      const decodedMessage: ServerMessage = JSON.parse(event.data);
      console.log(decodedMessage);

      if (decodedMessage.type === 'PREVIOUS_PIXELS') {
        decodedMessage.coordinates.forEach(c => {
          this.drawCircle(c.x, c.y, c.color);
        });
      }

      if (decodedMessage.type === 'NEW_PIXEL') {
        this.drawCircle(
          decodedMessage.circleCoordinates.x,
          decodedMessage.circleCoordinates.y,
          decodedMessage.circleCoordinates.color
        )
      }
    };
  }

  drawCircle(x: number, y: number, color: string) {
    const canvas: HTMLCanvasElement = this.canvas.nativeElement;
    const ctx = canvas.getContext('2d')!;
    const xCoordinate = x ;
    const yCoordinate = y ;
    const radius = 10;
    ctx.fillStyle = color;
    ctx.beginPath();
    ctx.arc(xCoordinate, yCoordinate, radius, 0, 2 * Math.PI);
    ctx.fill();
  }

  onCanvasClick(event: MouseEvent) {
    const x = event.offsetX;
    const y = event.offsetY;
    const color = this.color;
    this.ws.send(JSON.stringify({
      type: 'SEND_PIXEL',
      coordinates: {x, y, color}
    }));
  }

  getColor(e: Event) {
    this.color = (event!.target as HTMLInputElement).value
  }

  ngOnDestroy(): void {
    this.ws.close();
  }
}
